#  ws rds/variables.tf

variable "environment" {
  default = "prod"
}
variable managed_by {
  default = "rds"
}
