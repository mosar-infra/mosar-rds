#rds/outputs

output "rds" {
  value     = module.rds
  sensitive = true
}

output "secrets" {
  value     = module.secrets
  sensitive = true
}

