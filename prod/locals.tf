# ws rds /locals.tf

locals {
  default_egress = {
    default = {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}

locals {
  security_groups = {
    RDS = {
      name        = "rds_sg"
      vpc_id      = data.aws_vpc.vpc.id
      description = "Security group for private access to RDS"
      ingress_cidr = {
        http = {
          from        = 3306
          to          = 3306
          protocol    = "tcp"
          cidr_blocks = [for s in data.aws_subnet.private : s.cidr_block]
        }
      }
      ingress_sg = {}
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
  }
}

locals {
  secrets = {
    db-root-password_prod = {}
    db-username_prod      = {}
    db-user-flagr_prod    = {}
    db-pass-flagr_prod    = {}
  }
}

locals {
  private_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*private*"]
  }]
}
locals {
  env_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
  }]
}
