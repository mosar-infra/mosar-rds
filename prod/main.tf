#  ws rds/main.tf

module "rds" {
  source                              = "git::https://gitlab.com/mosar-infra/tf-module-rds.git?ref=tags/v1.0.8"
  db_storage                          = 10
  db_engine                           = "mysql"
  db_engine_version                   = "8.0.25"
  db_instance_class                   = "db.t4g.micro"
  db_name                             = "flashcards"

  db_password                         = module.secrets.secrets_versions["db-root-password_prod"].secret_string
  db_username                         = "root"
  db_subnet_group_name                = aws_db_subnet_group.mosar_rds_subnet_group.name
  vpc_security_group_ids              = module.security_groups.security_groups["RDS"].*.id
  db_identifier                       = "mosar-db-${var.environment}"
  skip_final_snapshot                 = false
  multi_az                            = true
  maintenance_window                  = "Mon:00:00-Mon:01:00"
  final_snapshot_identifier           = "mosar-db-backup-${var.environment}"
  deletion_protection                 = false
  backup_retention_period             = 7
  enabled_cloudwatch_logs_exports     = ["audit", "error", "general", "slowquery"]
  iam_database_authentication_enabled = true
}

module "security_groups" {
  source          = "git::https://gitlab.com/mosar-infra/tf-module-security-groups.git?ref=tags/v1.2.2"
  environment     = var.environment
  managed_by      = var.managed_by
  security_groups = local.security_groups
}

resource "aws_db_subnet_group" "mosar_rds_subnet_group" {
  name       = "mosar_rds_subnet_group_${var.environment}"
  subnet_ids = [for s in data.aws_subnet.private : s.id]
  tags = {
    Name        = "mosar_rds_sng"
    Environment = var.environment
    ManagedBy   = var.managed_by
  }
}

module "secrets" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-secrets.git?ref=tags/v3.1.2"
  secrets     = local.secrets
  environment = var.environment
  managed_by  = var.managed_by
}
