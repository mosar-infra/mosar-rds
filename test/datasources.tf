#rds/main/datasources

data "aws_vpc" "mosar_test" {
  dynamic "filter" {
    for_each = local.env_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnets" "private" {
  dynamic "filter" {
    for_each = local.private_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}
data "aws_subnet" "private" {
  for_each = toset(data.aws_subnets.private.ids)
  id       = each.value
}
